let adminUser = localStorage["isAdmin"];
let adminButton = document.querySelector('#adminButton')
let cardFooter;
let url;


const params = new URLSearchParams(window.location.search);
const courseId = params.get('courseId');
let token = localStorage.getItem('token');


// show logout button if logged in
let logoutLink = document.querySelector('#logout')

if(!token) {
	
		logoutLink.innerHTML = ""
	
} else { logoutLink.innerHTML = `
		<ul class="navbar-nav ms-auto">
	        <li class="nav-item">
	          	<a href="./logout.html" class="nav-link">Logout</a>
	        </li>
    	</ul>
	`
}
//logout button end

if (adminUser == "false" || !adminUser) {
	adminButton.innerHTML = ""
	// will display only active courses
	url ='https://mavt-course-booking-app.herokuapp.com/api/courses/';

	}else {
		adminButton.innerHTML = `
		<div class="col-md-2">
	     <a href="./addCourse.html" class="btn btn-primary">Add Course</a>
		  </div>
		`
		// will display all courses regardless of status
		url = 'https://mavt-course-booking-app.herokuapp.com/api/courses/all';
}


fetch(url)
.then(res => {
	return res.json()
})
.then(data =>{
	
	function displayCardFooter(courseId){
		if (adminUser == "false" || !adminUser) {
			cardFooter = `<a href="./course.html?courseId=${courseId}" class="btn btn-primary editButton">Select Course</a>`
		} else {
			cardFooter = `	
			
			<a href="./editCourse.html?courseId=${courseId}" class="btn btn-primary editButton">Edit
			</a>
			
			<a href="./courseDetails.html?courseId=${courseId}" class="btn btn-warning detailsButton">Details About the Course</a>
			`
		}

		return cardFooter;
	}
	

	let courseContainer = document.querySelector('#courseContainer');
	let courseData = data.map( elem => {
			
			return `
				
				<div class="col-md-6 my-3">
					
					<div id="cardcourses" class="card">
						<div class="card-body">
							<h5 class="card-title">${elem.name}</h5>
							
							<p class="card-text text-right">&#8369; ${elem.price}</p>
							<p class="card-text">${elem.description}</p>
						</div>

						<div class="card-footer">
							${displayCardFooter(elem._id)}
						</div>
					</div>
				</div>
			`
	})




	// console.log(courseData.join(""))
	courseContainer.innerHTML = courseData.join("");

})

