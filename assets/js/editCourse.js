const urlParams = new URLSearchParams(window.location.search);
const courseId = urlParams.get('courseId');
let token = localStorage.getItem('token');
//has method checks if the courseId key exists in url query string
//returns boolean
// console.log(params.has('courseId'))

let url = `https://mavt-course-booking-app.herokuapp.com/api/courses/${courseId}`;

let name = document.querySelector('#courseName')
let price = document.querySelector('#coursePrice')
let description = document.querySelector('#courseDescription')

fetch(url)
.then(res => res.json())
.then(data => {

	console.log(data)

	name.value = data.name;
	price.value = data.price;
	description.value = data.description;
	//toggle switch button
	document.querySelector('#toggleSwitch').checked = data.isActive



		// when user clicks Submit button
		let editCourse = document.querySelector('#editCourse');

		editCourse.addEventListener('submit', (e) => {
		e.preventDefault();


		let courseName = name.value
		let courseDesc = description.value
		let coursePrice = price.value
		let token = localStorage.getItem('token')
		


		fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses`,{
				method:"PUT",
				headers : {
					"Content-Type":"application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					_id: courseId,
					name: courseName,
					description: courseDesc,
					price: coursePrice
				})
			})
			.then( res => res.json())
			.then( data => {
				// console.log(data)
				if(data ===true) {
					window.location.replace("./courses.html")
				} else {
					alert("Something went wrong");
					}

			})

		})

		// for toggle switch
		let input = document.getElementById('toggleSwitch');
		let output = document.getElementById('toggleStatus');

		console.log(input)
			input.addEventListener('change',function () {
				if(this.checked) {
					output.innerHTML = 'Active';

					fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/activate/${courseId}`,{
							method:"PUT",
							headers : {
								"Content-Type":"application/json",
								"Authorization": `Bearer ${token}`
							}
							
						})
						.then(res => res.json())
						.then(data => {

							console.log(data)
							if(data ===true) {
								alert('Course is now ACTIVE.')
								window.location.replace("./courses.html")
							} else {
								alert("Something went wrong");
								}
						})

				} else {
					output.innerHTML = 'Inactive';

					fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/${courseId}`, {
								method: 'DELETE',
								headers: {
									'Authorization': `Bearer ${token}`
								}
							})
							.then(res => res.json())
							.then(data => {
								
								alert('This course is now DISABLED.');
								
							})

				}
			})
		// end of toggle


		//if course is disbled
		// let enableCourse = document.querySelector('#enableCourseButton')
	
		// if(data.isActive === false) {
		// 	 enableCourse.innerHTML = `
		// 		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
	 //            	<button class="btn btn-success me-md-2" id ="enableCourse" type="button">Enable this Course</button>
	 //        	</div>
		// 	`
		// } else { enableCourse.innerHTML= ""}

		// enableCourse.addEventListener('click', (e) => {
		// 	e.preventDefault(); 
			

		// 	fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/activate/${courseId}`,{
		// 		method:"PUT",
		// 		headers : {
		// 			"Content-Type":"application/json",
		// 			"Authorization": `Bearer ${token}`
		// 		}
				
		// 	})
		// 	.then(res => res.json())
		// 	.then(data => {

		// 		console.log(data)
		// 		if(data ===true) {
		// 			window.location.replace("./courses.html")
		// 		} else {
		// 			alert("Something went wrong");
		// 			}
		// 	})


		// })

		// ===end for activation
})
