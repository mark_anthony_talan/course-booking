const urlParams = new URLSearchParams(window.location.search);
const courseId = urlParams.get('courseId');

let url = `https://mavt-course-booking-app.herokuapp.com/api/courses/${courseId}`;

let token = localStorage.getItem('token');


// show logout button if logged in
let logoutLink = document.querySelector('#logout')

if(!token) {
	
		logoutLink.innerHTML = ""
	
} else { logoutLink.innerHTML = `
		<ul class="navbar-nav ms-auto">
	        <li class="nav-item">
	          	<a href="./logout.html" class="nav-link">Logout</a>
	        </li>
    	</ul>
	`
}
//end


fetch(url)
.then(res => res.json())
.then(data =>{
	// console.log(data)

	let courseName = document.querySelector('#courseName')
	let courseDesc = document.querySelector('#courseDesc')
	let coursePrice = document.querySelector('#coursePrice')

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;


	document.querySelector('#enrollButton').addEventListener('click', () => {

		if (!token) {
			alert("It seems you are not yet logged in.")
			window.location.replace('./login.html')
		} else {

			fetch(`https://mavt-course-booking-app.herokuapp.com/api/users/enroll`,{
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage["token"]}`
			},
			body: JSON.stringify({
				courseId : courseId
			})
			})
			.then( res => res.json())
			.then( data => {
				console.log(data)
				if (data === true) {
					alert('Thank you for enrolling! See you in class!');
					window.location.replace('./courses.html');
				} else {
					alert('Something went wrong');
				}
			})
		}
	})

})
