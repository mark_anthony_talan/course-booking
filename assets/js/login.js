let logInUser = document.querySelector('#logInUser');
logInUser.addEventListener('submit', (e) => {
	e.preventDefault();

let email = document.querySelector('#email').value;
let password = document.querySelector('#password').value;

// let body = {
// 	email: email,
// 	password: password
// }

let url ="https://mavt-course-booking-app.herokuapp.com/api/users/login/"
let options = {
	method:"POST",
		body: JSON.stringify({
			email: email,
			password: password
		}),
		headers : {
			"Content-Type":"application/json"
		}
	}

	fetch(url,options)
	.then (res => res.json())
	.then (data => {
		// console.log(data)
		if (!data) return alert("Something went wrong");

		//store in local storage
		localStorage.setItem("token",data.accessToken)

		fetch('https://mavt-course-booking-app.herokuapp.com/api/users/details/', {
			headers: {
				"Authorization" : `Bearer ${localStorage["token"]}`
			}
		})
		.then( res => res.json())
		.then( user => {
			console.log(user)

			//id
			//isAdmin
			localStorage["id"] = user._id
			localStorage["isAdmin"] = user.isAdmin

			if(localStorage["isAdmin"]=='true') {
				window.location.replace('./courses.html')
			}
				else {
					window.location.replace('./profile.html')
				}
			
		})

	})

})


