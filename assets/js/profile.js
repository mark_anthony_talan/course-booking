let token = localStorage.getItem("token");
// let profileContainer = document.querySelector('#profileContainer');
const userDetailsContainer = document.querySelector('#userDetails');
const courseDetailsContainer = document.querySelector('#courseDetails');


if(!token || token === null){
	alert('You must login first!')
	window.location.href="./login.html"
} else {
	fetch('https://mavt-course-booking-app.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		userDetailsContainer.innerHTML = `
			<h3>First Name: ${data.firstName}</h3>
 			<h3>Last Name: ${data.lastName}</h3>
 			<h3>Email : ${data.email}</h3>
 			<h3 class="mt-3">Class History</h3>
		`
			//console.log(data);

		//show enrolled courses
		data.enrollments.forEach(course => {
			fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/${course.courseId}`)
			.then( res => res.json())
			.then( data => {
				let date = new Date(course.enrolledOn);

				const tr = document.createElement('tr');
				tr.innerHTML = `
					<td>${data.name}</td>
					<td>${date}</td>
					<td>${course.status}</td>
				`
				courseDetailsContainer.appendChild(tr);
			})
		})
	})
	.catch(err => console.log(err))
}