const params = new URLSearchParams(window.location.search);
// console.log(params.get('courseId'));
const courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)
})