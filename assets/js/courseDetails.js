const urlParams = new URLSearchParams(window.location.search);
const courseId = urlParams.get('courseId');

let token = localStorage.getItem("token");


const userDetailsContainer = document.querySelector('#userDetails');
const courseDetailsContainer = document.querySelector('#courseDetails');


if(!token || token === null){
	alert('You must login first!')
	window.location.href="./login.html"
} 
	else {
		fetch(`https://mavt-course-booking-app.herokuapp.com/api/courses/${courseId}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			courseDetailsContainer.innerHTML = `
				<h3>Course Name: ${data.name}</h3>
	 			<h3>Description: ${data.description}</h3>
	 			<h3>Price : ${data.price}</h3>
			`
			//show enrolled courses
			data.enrollees.forEach(user => {
				fetch(`https://mavt-course-booking-app.herokuapp.com/api/users/${user.userId}`)
				.then( res => res.json())
				.then( data => {
					let date = new Date(user.enrolledOn);

					const tr = document.createElement('tr');
					tr.innerHTML = `
						<td>${data.firstName}</td>
						<td>${data.lastName}</td>
						<td>${date}</td>
					`
					userDetailsContainer.appendChild(tr);
				})
			})

		})
		.catch(err => console.log(err))
	}