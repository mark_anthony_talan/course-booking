let token = localStorage.getItem('token');

// show logout button if logged in
let navigate = document.querySelector('#linksForNotLoggedIn')

if(!token) {
	
		navigate.innerHTML = `
			<li class="nav-item">
          		<a href="pages/login.html" class="nav-link">Login</a>
          	</li>
          	<li class="nav-item">
          		<a href="pages/register.html" class="nav-link">Register</a>
          	</li>
		`
	
	} else { navigate.innerHTML = ""
}
//end