// make create course feature usable
// if successful in creating course, redirect user in courses.html
// otherwise alert ("Something went wrong")


let createCourse = document.querySelector('#createCourse');
createCourse.addEventListener('submit', (e) => {
	e.preventDefault();

let name = document.querySelector('#name').value;
let price = document.querySelector('#price').value;
let description = document.querySelector('#description').value;

let url ="https://mavt-course-booking-app.herokuapp.com/api/courses/"
let options = {
		method:"POST",
		body: JSON.stringify({
			name: name,
			price: price,
			description: description
		}),
		headers : {
			"Content-Type":"application/json",
			"Authorization" : `Bearer ${localStorage["token"]}`
		}
}

fetch(url,options)
.then (res => {
	return res.json()
})
.then (data => {
	if(!data) return alert("Something went wrong")
		// console.log("Added course")
	window.location.replace("./courses.html")
		
})

})